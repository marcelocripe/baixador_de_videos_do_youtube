pt-BR:

O programa "Baixador de Vídeos do YouTube" foi criado por RJP.

Contém: o arquivo ou pacote de instalação ".deb" e um arquivo de texto em idioma "pt-BR" explicando como utilizá-lo.

Baixe ou descarregue ou transfira os arquivos "youtube-downloader_1_all.deb", "youtube-downloader_1_all.deb.md5.sum", "youtube-downloader_1_all.deb.sha256.sum" e o arquivo de texto ".txt".

Todos os créditos e direitos estão incluídos nos arquivos, em respeito ao trabalho voluntário de cada pessoa que participou e colaborou para que estes arquivos pudessem ser disponibilizados nesta página eletrônica.

marcelocripe

- - - - -

de:

Das Programm „YouTube Video Downloader“ wurde von RJP erstellt.

Es enthält: die .deb-Installationsdatei oder das Paket und eine Textdatei in "pt-BR"-Sprache, die erklärt, wie man es benutzt.

Laden Sie die Dateien „youtube-downloader_1_all.deb“, „youtube-downloader_1_all.deb.md5.sum“, „youtube-downloader_1_all.deb.sha256.sum“ und die Textdatei „.txt“.

Alle Credits und Rechte sind in den Dateien enthalten, in Bezug auf die freiwillige Arbeit jeder Person, die teilgenommen und mitgearbeitet hat, damit diese Dateien auf dieser Website verfügbar gemacht werden konnten.

marcelocripe

- - - - -

en:

"YouTube Video Downloader" program was created by RJP.

It contains: the ".deb" installation file or package and a text file in "pt-BR" language explaining how to use it.

Download the files "youtube-downloader_1_all.deb", "youtube-downloader_1_all.deb.md5.sum", "youtube-downloader_1_all.deb.sha256.sum" and the text file ".txt".

All credits and rights are included in the files, in respect for the voluntary work of each person who participated and collaborated so that these files could be made available on this website.

marcelocripe

- - - - -

es:

El programa "YouTube Video Downloader" fue creado por RJP.

Contiene: el archivo o paquete de instalación ".deb" y un archivo de texto en idioma "pt-BR" que explica cómo usarlo.

Descarga los archivos "youtube-downloader_1_all.deb", "youtube-downloader_1_all.deb.md5.sum", "youtube-downloader_1_all.deb.sha256.sum" y el archivo de texto ".txt".

Todos los créditos y derechos están incluidos en los archivos, en respeto al trabajo voluntario de cada persona que participó y colaboró ​​para que estos archivos pudieran estar disponibles en este sitio web.

marcelocripe

- - - - -

fi:

"YouTube Video Downloader" -ohjelman loi RJP.

Se sisältää: ".deb"-asennustiedoston tai -paketin ja tekstitiedoston "pt-BR"-kielellä, joka selittää kuinka sitä käytetään.

Lataa tiedostot "youtube-downloader_1_all.deb", "youtube-downloader_1_all.deb.md5.sum", "youtube-downloader_1_all.deb.sha256.sum" ja tekstitiedosto ".txt".

Kaikki hyvitykset ja oikeudet sisältyvät tiedostoihin jokaisen osallistuneen ja yhteistyöhön osallistuneen henkilön vapaaehtoistyön osalta, jotta nämä tiedostot voitaisiin asettaa saataville tällä verkkosivustolla.

marcelocripe

- - - - -

fr :

Le programme "YouTube Video Downloader" a été créé par RJP.

Il contient : le fichier ou package d'installation ".deb" et un fichier texte en langage "pt-BR" expliquant comment l'utiliser.

Téléchargez les fichiers "youtube-downloader_1_all.deb", "youtube-downloader_1_all.deb.md5.sum", "youtube-downloader_1_all.deb.sha256.sum" et le fichier texte ".txt".

Tous les crédits et droits sont inclus dans les fichiers, dans le respect du travail bénévole de chaque personne qui a participé et collaboré afin que ces fichiers puissent être mis à disposition sur ce site.

marcelocripe

- - - - -

it:

Il programma "YouTube Video Downloader" è stato creato da RJP.

Contiene: il file o pacchetto di installazione ".deb" e un file di testo in linguaggio "pt-BR" che ne spiega l'utilizzo.

Scarica i file "youtube-downloader_1_all.deb", "youtube-downloader_1_all.deb.md5.sum", "youtube-downloader_1_all.deb.sha256.sum" e il file di testo ".txt".

Tutti i crediti ei diritti sono inclusi nei file, nel rispetto del lavoro volontario di ogni persona che ha partecipato e collaborato affinché questi file potessero essere resi disponibili su questo sito web.

marcelocripe
